#!/bin/bash
set -u
set -e

which jq curl >/dev/null

json="$(curl -s 'https://pass.telekom.de/api/service/generic/v1/status')"
json="$(cat antwort.json)"

tarif=$(jq -r .passName <<<$json)
passName=$(jq -r .passName <<<$json)
remainingseconds=$(jq -r .remainingSeconds <<<$json)
remainingseconds=100000

let remainingdays='remainingseconds/86400' || true


echo passName $passName
echo remainingdays $remainingdays
#echo $json
echo "ENDE"
